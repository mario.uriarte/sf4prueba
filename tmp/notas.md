Comandos para sf4
=================

## symfony
iniciar proyecto
```bash
composer create-project symfony/skeleton proyecto_asombroso
composer require server
composer require annotations
composer require sec-checker
composer require twig
composer require profiler --dev

#https://knpuniversity.com/screencast/symfony/debugging-packs#play
composer require debug --dev
composer require asset

```

Crear un CRUD

```bash
composer require doctrine
composer require maker --dev
php bin/console make:crud
```

## doctrine
https://symfony.com/doc/current/doctrine.html

```bash
#https://knpuniversity.com/screencast/symfony/debugging-packs#play
```

```bash
php bin/console doctrine:database:create
```
Crear una entidad
```bash
php bin/console make:entity
```

Construir el sql con el bundle migration
```bash
php bin/console make:migration
```

Llenar la base de datos

```bash
php bin/console doctrine:migrations:migrate
```

Generar setters y getters, tambien hace lectura desde la estructura 
mapping, es decir leera los archivos yml y xml.
```bash
php bin/console make:entity --regenerate
```

borrar la base de datos y cargar la base de datos con los mappings
```bash
php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:update --force
```

## hautelook fixtures
hautelook/alice-bundle
hautelook/alice-bundle Symfony bundle to manage fixtures with Alice and Faker.
```bash
composer require --dev theofidry/alice-data-fixtures
composer require --dev doctrine/data-fixtures
composer require --dev nelmio/alice
composer require --dev hautelook/alice-bundle                                                                                
```

Añadir manualmente a los bundles
https://github.com/hautelook/AliceBundle/issues/380
```bash
    Hautelook\AliceBundle\HautelookAliceBundle::class => ['dev' => true, 'test' => true],
    Nelmio\Alice\Bridge\Symfony\NelmioAliceBundle::class => ['dev' => true, 'test' => true],
    Fidry\AliceDataFixtures\Bridge\Symfony\FidryAliceDataFixturesBundle::class => ['dev' => true, 'test' => true]
```
Llenar la base de datos con los fixtures de src/Resources/*.yml
```bash
php bin/console hautelook:fixtures:load -vvv
```
