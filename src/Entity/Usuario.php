<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario
{
    private $id;

    private $nombres;

    private $apellidos;

    private $email;

    private $password;

    private $articulos;

    private $apodos;

    private $foto;

    private $fechaCreacion;

    private $ultimoIngreso;

    public function __construct()
    {
        $this->articulos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombres.' '.$this->apodos;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticulos(): Collection
    {
        return $this->articulos;
    }

    public function addArticulo(Article $articulo): self
    {
        if (!$this->articulos->contains($articulo)) {
            $this->articulos[] = $articulo;
            $articulo->setUsuario($this);
        }

        return $this;
    }

    public function removeArticulo(Article $articulo): self
    {
        if ($this->articulos->contains($articulo)) {
            $this->articulos->removeElement($articulo);
            // set the owning side to null (unless already changed)
            if ($articulo->getUsuario() === $this) {
                $articulo->setUsuario(null);
            }
        }

        return $this;
    }

    public function getApodos(): ?string
    {
        return $this->apodos;
    }

    public function setApodos(?string $apodos): self
    {
        $this->apodos = $apodos;

        return $this;
    }

    public function getFoto(): ?string
    {
        return $this->foto;
    }

    public function setFoto(string $foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getUltimoIngreso(): ?\DateTimeInterface
    {
        return $this->ultimoIngreso;
    }

    public function setUltimoIngreso(?\DateTimeInterface $ultimoIngreso): self
    {
        $this->ultimoIngreso = $ultimoIngreso;

        return $this;
    }
}
