<?php
/**
 * Created by PhpStorm.
 * User: mario
 * Date: 19-04-18
 * Time: 12:27 PM
 */
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    /**
     * @Route("/")
     */
    public function homepage()
    {
        return new Response('OMG! Mi primera página en sf4');
    }
}
